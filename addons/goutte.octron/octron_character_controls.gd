extends Reference
class_name OctronCharacterControls


# Base class to extend.  Does nothing by default.
# Look in res://addons/goutte.octron/controls/ for examples.


# why u no want?
#var character:OctronCharacter
var character
#var camera:OctronCamera
var camera


#func inject(character:OctronCharacter, camera:OctronCamera):
func inject(character, camera):
	self.character = character
	self.camera = camera
	setup()


func setup():
	pass


func process_controls(_delta):
	# This will be called on each physics frame
	pass

