extends Mesh

# Truncated Octahedron (Octron, Mecon) Mesh Generator.
# 
# This uses SurfaceTool and some code to generate the mesh.
# It's not as efficient as loading the pre-computed `.shape`, `.obj` or `.mesh`.
# But it's more flexible. Hack away! :)
# 
# We do not deal with materials here.
# You can assign them later using `surface_set_material()`,
# or suggest a way of dealing with them here.
# 
# The squares are aligned with the XYZ axes.
# 
# The surfaces are created in the following order:
# When camera is looking towards -z from some point along +z,
# 00. Front Square
# 01. Back Square
# 02. Top Square
# 03. Bottom Square
# 04. Right Square
# 05. Left Square
# 06. Front Top Left Hexagon
# 07. Front Bottom Left Hexagon
# 08. Front Bottom Right Hexagon
# 09. Front Top Right Hexagon
# 10. Back Top Right Hexagon
# 11. Back Bottom Right Hexagon
# 12. Back Bottom Left Hexagon
# 13. Back Top Left Hexagon


# The unique vertices are all the permutations of (0, 0.5, 1) with negatives
# since they are the 24 permutations of (1, 2, 3, 4) in 4D.
# https://en.wikipedia.org/wiki/Permutohedron
# Not sure it makes much sense to sort them "alphabetically" here.
# So, we group them by square.  If you have a better idea…
# When camera is looking towards -z from some point along +z, it goes:
# front, back, top, bottom, right, left
#    +z,   -z,  +y,     -y,    +x,   -x
# That reverse ZYX order is to get the front square as first surface,
# which feels to make more sense in the context of Godot and video games.
# On each square the vertices are counter-clockwise (=trigwise)
# when seen from the outside, and the initial vertex is kind of arbitrary.
# Same goes: if you have a more elegant idea, please share!
const VERTICES = [
	# Front Square
	Vector3(  0.5,    0,    1 ),  # 00
	Vector3(    0,  0.5,    1 ),  # 01
	Vector3( -0.5,    0,    1 ),  # 02
	Vector3(    0, -0.5,    1 ),  # 03
	# Back Square
	Vector3( -0.5,    0,   -1 ),  # 04
	Vector3(    0,  0.5,   -1 ),  # 05
	Vector3(  0.5,    0,   -1 ),  # 06
	Vector3(    0, -0.5,   -1 ),  # 07
	# Top Square
	Vector3(  0.5,    1,    0 ),  # 08
	Vector3(    0,    1, -0.5 ),  # 09
	Vector3( -0.5,    1,    0 ),  # 10
	Vector3(    0,    1,  0.5 ),  # 11
	# Bottom Square
	Vector3(  0.5,   -1,    0 ),  # 12
	Vector3(    0,   -1,  0.5 ),  # 13
	Vector3( -0.5,   -1,    0 ),  # 14
	Vector3(    0,   -1, -0.5 ),  # 15
	# Right Square
	Vector3(    1,    0, -0.5 ),  # 16
	Vector3(    1,  0.5,    0 ),  # 17
	Vector3(    1,    0,  0.5 ),  # 18
	Vector3(    1, -0.5,    0 ),  # 19
	# Left Square
	Vector3(   -1,    0,  0.5 ),  # 20
	Vector3(   -1,  0.5,    0 ),  # 21
	Vector3(   -1,    0, -0.5 ),  # 22
	Vector3(   -1, -0.5,    0 ),  # 23
]

# Indices of the VERTICES const above.
# Points are trigwise when seen from the outside.
# The initial vertex is the one "on the right" (angle=0 in the trig circle).
const HEXAGONS_GEOMETRIES = [
	[ 01, 11, 10, 21, 20, 02 ],  # front top left
	[ 03, 02, 20, 23, 14, 13 ],  # front bottom left
	[ 19, 18, 00, 03, 13, 12 ],  # front bottom right
	[ 17, 08, 11, 01, 00, 18 ],  # front top right
	[ 05, 09, 08, 17, 16, 06 ],  # back top right
	[ 07, 06, 16, 19, 12, 15 ],  # back bottom right
	[ 23, 22, 04, 07, 15, 14 ],  # back bottom left
	[ 21, 10, 09, 05, 04, 22 ],  # back top left
]

const HEXAGONS_NORMALS = [
	Vector3( -1,  1,  1),  # front top left
	Vector3( -1, -1,  1),  # front bottom left
	Vector3(  1, -1,  1),  # front bottom right
	Vector3(  1,  1,  1),  # front top right
	Vector3(  1,  1, -1),  # back top right
	Vector3(  1, -1, -1),  # back bottom right
	Vector3( -1, -1, -1),  # back bottom left
	Vector3( -1,  1, -1),  # back top left
]

const SQUARES_NORMALS = [
	# Front Square
	Vector3(  0,  0,  1 ),
	# Back Square
	Vector3(  0,  0, -1 ),
	# Top Square
	Vector3(  0,  1,  0 ),
	# Bottom Square
	Vector3(  0, -1,  0 ),
	# Right Square
	Vector3(  1,  0,  0 ),
	# Left Square
	Vector3( -1,  0,  0 ),
]

# This UV mapping is the first, simplest UV mapping.
# We should also provide other UV mappings, like using a net. (but which?)
# All squares have the same UVs.
# All hexagons have the same UVs.
# The hexagon is flat-topped, and its lateral vertices touch the edges.
# Since in practice we use square texture bitmaps,
# that means the paddings top and bottom are each (2 - sqrt(3))/4 ≅ 6.7%
# 
#         _____ 
#        /     \
#       /       \
#      <         >
#       \       /
#        \_____/
#      0         1 
# 
# 


const HHUV_Y1 = 1.0 / 2.0
const HHUV_Y2 = (2 - sqrt(3)) / 4.0
const HHUV_X1 = 1.0 / 4.0
const HHUV_X2 = 3.0 / 4.0
const HOMOTILIC_HEXAGONS_UVS = [
	Vector2(1.0, HHUV_Y1),
	Vector2(HHUV_X2, HHUV_Y2),
	Vector2(HHUV_X1, HHUV_Y2),
	Vector2(0.0, HHUV_Y1),
	Vector2(HHUV_X1, 1.0 - HHUV_Y2),
	Vector2(HHUV_X2, 1.0 - HHUV_Y2),
]
const HOMOTILIC_SQUARE_UVS = [
	Vector2(1.0, 0.0),
	Vector2(0.0, 0.0),
	Vector2(0.0, 1.0),
	Vector2(1.0, 1.0),
]

func _init(multiple_surfaces=true):
	var surface_tool = SurfaceTool.new()
	
	# For each of the 6 squares…
	for sq_i in range(6):
		surface_tool.begin(VisualServer.PRIMITIVE_TRIANGLE_FAN)
		# For each of the 4 vertices of the square…
		for vx_i in range(4):
			var local_index = (3 - vx_i) # trigwise to clockwise
			var vertex_index = 4 * sq_i + local_index
			surface_tool.add_uv(get_square_uv_for_vertex(vertex_index, sq_i, local_index))
			surface_tool.add_normal(SQUARES_NORMALS[sq_i])
			surface_tool.add_vertex(VERTICES[vertex_index]) # after props like uv
		surface_tool.commit(self)
	
	# For each hexagon…
	for hx_i in range(HEXAGONS_GEOMETRIES.size()):
		surface_tool.begin(VisualServer.PRIMITIVE_TRIANGLE_FAN)
		for vx_i in range(6):
			var local_index = (5 - vx_i) # trigwise to clockwise
			var vertex_index = HEXAGONS_GEOMETRIES[hx_i][local_index]
			surface_tool.add_uv(get_hexagon_uv_for_vertex(vertex_index, hx_i, local_index))
			surface_tool.add_normal(HEXAGONS_NORMALS[hx_i])
			surface_tool.add_vertex(VERTICES[vertex_index]) # after props like uv
		surface_tool.commit(self)


# Override me?
func get_square_uv_for_vertex(vertex_index, square_index, local_index):
	return get_homotilic_square_uv_for_vertex(vertex_index, square_index, local_index)


# Override me?
func get_hexagon_uv_for_vertex(vertex_index, hexagon_index, local_index):
	return get_homotilic_hexagon_uv_for_vertex(vertex_index, hexagon_index, local_index)


func get_homotilic_square_uv_for_vertex(vertex_index, square_index, local_index):
	return HOMOTILIC_SQUARE_UVS[local_index]


func get_homotilic_hexagon_uv_for_vertex(vertex_index, hexagon_index, local_index):
	return HOMOTILIC_HEXAGONS_UVS[local_index]


func export_shape(shape_filepath, no_margins=false):
	var cps = ConvexPolygonShape.new()
	cps.set_points(VERTICES)
	if no_margins:
		cps.set_margin(0)
	ResourceSaver.save(shape_filepath, cps)


func export_shape2(shape_filepath):
	var cps = create_convex_shape()
	ResourceSaver.save(shape_filepath, cps)
