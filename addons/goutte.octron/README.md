 
## Truncated Octahedron Toolkit for Godot

Includes :

- [x] Truncated Octahedron Mesh (node)
- [x] Truncated Octahedron Shape (resource)
- [x] Truncated Octahedron MultiMesh (node)
- [x] Truncated Octahedron Lattice (helper)
- [ ] Truncated Octahedron OBJ (resource, homotilic UVs)
- [ ] Truncated Octahedron OBJ (resource, net UVs)
- [ ] Customizable UV mapping (right now you _can_ but it's hard)


## How to install

1. Copy into your Godot project.
2. Enable the plugin in project settings.

This project is not yet available in Godot's asset store.


## Usage Examples

### Collision Shape

```gdscript
var sb = StaticBody.new()
var cs = CollisionShape.new()
cs.shape = preload("res://addons/goutte.octron/geo/octron.shape")
sb.add_child(cs)
```

### Mesh Instance

Select `OctronMeshInstance` in the list of nodes.
The specs of the truncated octahedron are described below.

Remember to enable the plugin first ;)

> This could be much faster, since it (still) generates procedurally the mesh instead of loading it from a resource, whether embedded or external.


### Multi Mesh Instance

Select `OctronMultiMeshInstance` in the list of nodes.

```gdscript
const Octron = preload("res://addons/goutte.octron/octron.gd")
var on_cell = Vector3(42, 0, 0)
var octron = Octron.new()
$OctronMultiMeshInstance.add_octron(octron, on_cell)
```

> Be careful ; for performance reasons we'll probably keep the `OctronMultiMeshInstance` API low-level and naive, which means it will choke on invalid cell positions.  Use `OctronLattice` to check them.


## Specs of the provided Truncated Octahedron

The truncated octahedron has its square faces aligned with the XYZ axes.
Its width from square to square is 2, which means that the set of truncated octahedrons whose (x, y, z) integer position in 3D matches the following formula will tile space without gaps :

    (abs(x%2) == abs(y%2)) and (abs(z%2) == abs(y%2))

This formula is available in `OctronLattice.is_xyz_valid(x, y, z)`.


### Bounding Box

It also means that the bounding box has size (2, 2, 2).


### Neighbors

Each truncated octahedron has 14 neighbors.

They are in the following directions, for the hexagons:

    ( -1, -1, -1 )
    (  1, -1, -1 )
    ( -1,  1, -1 )
    ( -1, -1,  1 )
    (  1,  1,  1 )
    ( -1,  1,  1 )
    (  1, -1,  1 )
    (  1,  1, -1 )

And for the squares:
    
    (  2,  0,  0 )
    ( -2,  0,  0 )
    (  0,  2,  0 )
    (  0, -2,  0 )
    (  0,  0,  2 )
    (  0,  0, -2 ) 

