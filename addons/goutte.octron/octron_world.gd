extends Spatial

# More of a demo and blackboard for glue code than a reusable script.

# Although we're trying to ensure that methods may be overriden.

# Usage
# extends "res://addons/goutte.octron/octron_world.gd"

# WIP
# Handles
# - a Lattice
# - Character / Camera / Controls ?
# - MultiMeshe(s) for rendering the Octrons (using chunking, probably)


const Octron = preload("res://addons/goutte.octron/octron.gd")
const OctronCamera = preload("res://addons/goutte.octron/octron.camera.gd")
const OctronLattice = preload("res://addons/goutte.octron/octron_lattice.gd")
#const OctronCharacter = preload("res://addons/goutte.octron/octron_character.gd")
const OctronMeshInstance = preload("res://addons/goutte.octron/octron_mesh_instance.gd")
const OctronMultiMeshInstance = preload("res://addons/goutte.octron/octron_multi_mesh_instance.gd")
const OctronStaticBodyChunker = preload("res://addons/goutte.octron/octron_static_body_chunker.gd")


export(String, FILE, "*.tscn,*.scn") var character_scene_path = "res://addons/goutte.octron/octron_character.tscn"


var lattice = OctronLattice.new()
var should_process_inputs = false


func _init():
	init()

func init():
#	read_configuration()
#	connect_to_network()
	configure_lattice()


var is_ready = false
func _ready():
#	emit_signal("scene_ready")
#	return
	
	ready()

signal scene_ready # old test, not used

func ready():
	setup_characters()
	setup_camera()
	setup_controls()
	setup_chunker()
	generate_world()
	yield(get_tree(), "idle_frame")
	after_world_generation()
	enable_inputs()
	is_ready = true  # remove me and test
	emit_signal("scene_ready")

func enable_inputs():
	self.should_process_inputs = true


# Override this method in your own script extending this one
func configure_lattice():
	pass


var characters = Array()  # of OctronCharacters
var character  # currently controlled OctronCharacter
# Override this method in your own script extending this one
func setup_characters():
	if self.character:
		return
	var character_scene = load(self.character_scene_path)
	self.character = character_scene.instance()
	assert(self.character is OctronCharacter)  # we'll need its methods
#	self.character = OctronCharacter.new()
	self.character.can_sleep = false
	#self.character.name = "CharacterOctron"
	self.character.translate(Vector3(0,3,0))
	self.characters.append(self.character)
	add_child(self.character)
	

var camera:OctronCamera
# Override this method in your own script extending this one
func setup_camera():
	if self.camera:
		return
	self.camera = OctronCamera.new()
	self.camera.translate(Vector3(0,0,13))
	self.camera.set_current(true)
	
	if self.character:
		self.character.eye.add_child(self.camera)
	else:
		add_child(self.camera)

var controls:OctronCharacterControls
# Override this method in your own script extending this one
func setup_controls():
	use_control_class(OctronKatamariControls)


func use_control_class(control_class:Script):
	self.controls = control_class.new()
	self.controls.inject(self.character, self.camera)
	self.character.set_controls(self.controls)


var multimesh  # ← WIP, use chunks
var terrain_static_body_chunker
#var terrain_static_body  # Holds all the shapes (use chunks?!)
func setup_chunker():
	self.multimesh = OctronMultiMeshInstance.new()
	self.multimesh.set_name("TerrainMultiMeshInstance")
	add_child(self.multimesh)
	
	self.terrain_static_body_chunker = OctronStaticBodyChunker.new()
	self.terrain_static_body_chunker.chunk_size_x = 8
	self.terrain_static_body_chunker.chunk_size_y = 8
	self.terrain_static_body_chunker.chunk_size_z = 8
	self.terrain_static_body_chunker.set_name("TerrainStaticBodyChunker")
	add_child(self.terrain_static_body_chunker)
	


# Override this method in your own script extending this one
func generate_world():
	pass


# Override this method in your own script extending this one
func after_world_generation():
#	add_child(self.terrain_static_body)
	pass


func add_octron(where):
	if lattice.can_add_thing(where):
		self.multimesh.add_octron(where)
		add_octron_collision_shape(where)
		var octron = Octron.new()
		lattice.add_thing(octron, where)
	else:
		printerr("Another octron is in the way.  How did you do that?")


var ray_length = 100

func find_octron(cursor_pos):
	var from = camera.project_ray_origin(cursor_pos)
	var to = from + camera.project_ray_normal(cursor_pos) * ray_length
	var space_state = get_world().direct_space_state
	var result = space_state.intersect_ray(from, to)
	prints("Found octron:", result)
	if result and result.has('collider') and result.has('shape'):
		var cell = self.terrain_static_body_chunker.find_cell(
			result['collider'], result['shape']
		)
		if null != cell:
			result['cell'] = cell
	return result


func find_ghost_cell_under_mouse(mouse_pos):
	var octron = find_octron(mouse_pos)
	if not octron:
		return null
	var found_direction = null
	var normal = octron['normal']
	for i in range(OctronLattice.DIRECTIONS.size()):
		var direction = OctronLattice.DIRECTIONS[i]
		if (direction.normalized() - normal).length_squared() < 0.01:
			found_direction = i
			break
	if null == found_direction:
		printerr("Direction not found : %s." % [normal])
		return null
	if not octron.has('cell'):
		printerr("Found collider is not an octron : %s." % [octron])
		return null
		
	return octron['cell'] + OctronLattice.DIRECTIONS[found_direction]


func remove_octron(on_cell):
	self.multimesh.remove_octron(on_cell)
	self.lattice.remove_things(on_cell)
	self.terrain_static_body_chunker.remove_octron(on_cell)


func add_octron_collision_shape(where):
	self.terrain_static_body_chunker.add_octron(where)


################################################################################
## TODO: Move to OctronCharacter and signals ###################################

# Clicks dragging further than sqrt(treshold) are ignored here
var _mouse_drift_treshold = 4
var _mouse_left_pressed_pos = Vector2()
var _mouse_right_pressed_pos = Vector2()
func _input(event):
	if not is_ready:
		return
	process_terraforming_inputs(event)
	process_other_inputs(event)

func process_terraforming_inputs(event):
	if (event is InputEventMouseButton) and (event.button_index == BUTTON_LEFT):
		var mouse_pos = get_viewport().get_mouse_position()
		if not event.pressed:
			if (mouse_pos - _mouse_left_pressed_pos).length_squared() < 5:
				var ghost_cell = find_ghost_cell_under_mouse(mouse_pos)
				if null != ghost_cell:
					add_octron(ghost_cell)
		_mouse_left_pressed_pos = mouse_pos
	if (event is InputEventMouseButton) and (event.button_index == BUTTON_RIGHT):
		var mouse_pos = get_viewport().get_mouse_position()
		if not event.pressed:
			if (mouse_pos - _mouse_right_pressed_pos).length_squared() < 5:
				var octron = find_octron(mouse_pos)
				if octron and octron.has('cell'):
					assert(octron.has('cell'))  # or in the if?
					var on_cell = octron['cell']
					remove_octron(on_cell)
		_mouse_right_pressed_pos = mouse_pos

func process_other_inputs(event):
	pass

func _process(delta):
	if not is_ready:
		return
	process(delta)

func process(delta):
	pass

func _physics_process(delta):
	if not is_ready:
		return
	physics_process(delta)

func physics_process(delta):
	if should_process_inputs:
		process_character_controls(delta)

func process_character_controls(delta):
	self.character.process_controls(delta)
