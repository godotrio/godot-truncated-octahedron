
## Truncated Octahedron Toolkit for Godot

Since this file is usually deleted when adding this plugin to Godot,
please read [`addons/goutte.octron/README.md`](./addons/goutte.octron/README.md) instead.

